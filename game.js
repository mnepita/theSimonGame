//Levels
var level = 0;

var buttonColours = ["red", "blue", "green", "yellow"];

var soundsObject = {
    red: "sounds/red.mp3",
    blue: "sounds/blue.mp3",
    green: "sounds/green.mp3",
    yellow: "sounds/yellow.mp3",
    wrong: "sounds/wrong.mp3"
}

var selectedButton = 0;

// create new Patterns
var gamePattern = [];
var userClickedPattern = [];


/* Start the game by pressing any keys */
$(document).keypress(function () {
    if (level == 0) {
        nextSequence();
    }
});

// nextSequence function
function nextSequence() {

    // Once nextSequence() is triggered, reset the userClickedPattern to an empty array ready for the next level.

    userClickedPattern = [];

    //level up & update title
    level++;
    $("#level-title").text("Level " + level);

    //creates a random number.
    var randomNumber = Math.floor(Math.random() * 4);
    var randomChosenColour = buttonColours[randomNumber];

    //pushes the select randomly generated color to the gamePatter array
    gamePattern.push(randomChosenColour);

    // animates the selected button base on its color
    selectedButton = $('#' + randomChosenColour).fadeIn(100).fadeOut(100).fadeIn(100);

    //plays audio matching selected button
    var audio = new Audio(soundsObject[buttonColours[randomNumber]]);
    // var audio = new Audio("sounds/" + randomChosenColour + ".mp3");
    audio.play();

    return selectedButton;

}

//Detect user's click events
$(".btn").on("click", function (event) {
    var userChosenColour = this.id;

    $('#' + userChosenColour).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
    userClickedPattern.push(userChosenColour);

    animatePress(userChosenColour);

    var audio = new Audio(soundsObject[userChosenColour]);
    audio.play();//refactor this code to be reusable when nextSequence runs & animation


    // Call checkAnswer() after a user has clicked and chosen their answer, passing in the index of the last answer in the user's sequence.
    checkAnswer(userClickedPattern.length - 1);

});

// animate function //
function animatePress(currentCoulor) {
    $('#' + currentCoulor).addClass("pressed").delay(100).removeClass("pressed");
}

// Checking users Answer //
function checkAnswer(currentLevel) {

    //3. Write an if statement inside checkAnswer() to check if the most recent user answer is the same as the game pattern. If so then log "success", otherwise log "wrong".

    if (gamePattern[currentLevel] === userClickedPattern[currentLevel]) {
        console.log("Same Index");

        //4. If the user got the most recent answer right in step 3, then check that they have finished their sequence with another if statement.

        if (gamePattern.length === userClickedPattern.length) {
            console.log("they are the same length");

            //5. Call nextSequence() after a 1000 millisecond delay.
            setTimeout(nextSequence, 1000)
        }
    } else {
        console.log("wrong index");
        // Step 9 - Game Over
        // 1. In the sounds folder, there is a sound called wrong.mp3, play this sound if the user got one of the answers wrong.
        var audio = new Audio(soundsObject["wrong"]);
        audio.play();//refactor this code to be reusable when nextSequence runs & animation

        $("#level-title").text("Game Over, Press Any Key to Restart");

        $('body').addClass("game-over");
        setTimeout(function () {
            $("body").removeClass("game-over");
        }, 500);

        startOver();

    }
};

// Restart the game
function startOver() {

    //reset the values of level, gamePattern and started variables.
    level = 0;
    gamePattern = [];

}